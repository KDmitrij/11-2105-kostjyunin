package service.decorator;

import service.OrderService;

public class LogDecorator extends Decorator{
    protected LogDecorator(OrderService needSleep) {
        super(needSleep);
    }

    @Override
    public String getOrder(String orderNumber) throws Exception {
        System.out.println("Method : getOrder");
        System.out.println("Params : [(String)orderNumber:" + orderNumber + "]");
        return super.getOrder(orderNumber);
    }

    @Override
    public String addOrder(String orderNumber, Long amount) {
        System.out.println("Method : addOrder");
        System.out.println("Params : [(String)orderNumber:" + orderNumber + ", (Long)amount:" + amount + "]");

        return super.addOrder(orderNumber, amount);
    }
}
