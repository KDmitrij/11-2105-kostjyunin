package service.decorator;

import service.OrderService;

public class ThrowDecorator extends Decorator{
    public ThrowDecorator(OrderService needSleep) {
        super(needSleep);
    }

    @Override
    public String getOrder(String orderNumber) throws Exception {
        try{
            return super.getOrder(orderNumber);
        }catch (Exception e){
            System.out.println("Exception: " + e.toString());
        }
        return null;
    }

}
