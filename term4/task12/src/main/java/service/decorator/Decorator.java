package service.decorator;

import service.OrderService;

/**
 * Created by связной on 26.03.14.
 */
public abstract class Decorator implements OrderService{
    private OrderService component;

    protected Decorator(OrderService needSleep) {
        this.component = needSleep;
    }

    @Override
    public String getOrder(String orderNumber) throws Exception {
        return component.getOrder(orderNumber);
    }

    @Override
    public String addOrder(String orderNumber, Long amount) {
        return component.addOrder(orderNumber, amount);
    }
}
