package service.decorator;

import service.OrderService;

public class TimeDecorator extends Decorator{
    public TimeDecorator(OrderService needSleep) {
        super(needSleep);
    }

    @Override
    public String addOrder(String orderNumber, Long amount) {
        System.out.println("Start addOrder():" + System.currentTimeMillis());
        String result= super.addOrder(orderNumber, amount);
        System.out.println("End addOrder():" + System.currentTimeMillis());
        return result;
    }
}
