package core;

import service.OrderService;

public class App {
	public static void main(String[] args) throws Exception {

		//ApplicationContext appContext =
       //         new ClassPathXmlApplicationContext("Spring-Customer.xml");
        Factory factory = Factory.getInstance();
		/*
		CustomerService customer = (CustomerService) factory.getBean("customerService");
		customer.addCustomer();
		
	    customer.addCustomerReturnValue();
		
		customer.addCustomerAround("customer#1");

        customer.addCustomerThrowException();*/

        OrderService orderService = (OrderService) factory.getBean("orderService");
        orderService.getOrder("111");
        orderService.addOrder("111", 1l);

	}
}