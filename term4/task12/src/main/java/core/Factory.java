package core;

import service.decorator.LogDecorator;
import service.decorator.ThrowDecorator;
import service.decorator.TimeDecorator;
import service.impl.CustomerServiceImpl;
import service.impl.OrderServiceImpl;
import service.proxy.CustomerServiceProxy;

public class Factory {

    private static Factory factory = new Factory();

    private Factory() {}

    public static Factory getInstance() {
        return factory;
    }

    public Object getBean(String name) {
        if(name.equals("customerService")) {
            return new CustomerServiceProxy(new CustomerServiceImpl());
        }
        if(name.equals("orderService")) {
            return new LogDecorator(new ThrowDecorator(new TimeDecorator(new OrderServiceImpl()))) {
            };
        }

        throw new IllegalArgumentException();
    }
}
