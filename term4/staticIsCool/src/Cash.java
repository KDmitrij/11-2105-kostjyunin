public class Cash {
    private float rub;
    /*
    Так как изменение цен затрагивает все счета пользователей, то лучше курс вынести в статик поле,
     это позволит поменять курс сразу у всех счетов
    */
    private static float exchangeEuro = 40.5f;
    private static float exchangeDollars = 34.5f;


    public Cash(float rub){
        this.rub=rub;
    }

    public float getRub() {
        return rub;
    }

    public void setRub(float rub) {
        this.rub = rub;
    }

    public float getExchangeEuro() {
        return rub/exchangeEuro;
    }

    public float getExchangeDollars() {
        return rub/exchangeDollars;
    }

    public static void setExchangeDollars(float exchangeDollars) {
        Cash.exchangeDollars = exchangeDollars;
    }

    public static void setExchangeEuro(float exchangeEuro) {
        Cash.exchangeEuro = exchangeEuro;
    }
}
