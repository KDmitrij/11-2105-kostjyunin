import java.util.Random;

public class Bank {
    public static void main(String[] args) {
        Cash user1 = new Cash(new Random().nextFloat()*10000);
        System.out.println(user1.getRub());
        System.out.println(user1.getExchangeDollars());
        System.out.println(user1.getExchangeEuro());
        //Допустим курс изменился
        Cash.setExchangeDollars(38.3f);
        Cash.setExchangeEuro(45.3f);
        System.out.println(user1.getRub());
        System.out.println(user1.getExchangeDollars());
        System.out.println(user1.getExchangeEuro());
        //Курс изменился без обращения к конкретному экземпляру, статика это круто
    }
}
