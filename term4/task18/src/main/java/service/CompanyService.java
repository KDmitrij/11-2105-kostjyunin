package service;

import model.Company;
import model.Vacancy;

import java.util.List;

public interface CompanyService {
    Vacancy getVacancyById(Long id);

    Company getCompanyById(Long id);

    Iterable<Vacancy> getAllVacancies();

    void saveVacancy(Vacancy vacancy);

    Iterable<Vacancy> getVacancyListByCategoryId(Long categoryID);


    Iterable<Vacancy> getVacanciesByNamePart(String term);

    List<String> getTitleByNamePart(String term);
}
