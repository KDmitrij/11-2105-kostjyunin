package service.impl;

import model.Company;
import model.Vacancy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.CompanyRepository;
import repository.VacancyRepository;
import service.CompanyService;

import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    private CompanyRepository companyRepository;
    private VacancyRepository vacancyRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository, VacancyRepository vacancyRepository) {
        this.companyRepository = companyRepository;
        this.vacancyRepository = vacancyRepository;
    }

    @Override
    public Vacancy getVacancyById(Long id) {
        return vacancyRepository.findOne(id);
    }

    @Override
    public Company getCompanyById(Long id) {
        return companyRepository.findOne(id);
    }

    @Override
    public Iterable<Vacancy> getAllVacancies() {
        return vacancyRepository.findAll();
    }

    @Override
    public void saveVacancy(Vacancy vacancy) {
        vacancyRepository.save(vacancy);
    }

    @Override
    public Iterable<Vacancy> getVacancyListByCategoryId(Long categoryId) {
        return vacancyRepository.findByCategory(categoryId);
    }

    @Override
    public Iterable<Vacancy> getVacanciesByNamePart(String term) {
        return vacancyRepository.findByTitleStartingWithIgnoreCase(term);
    }

    @Override
    public List<String> getTitleByNamePart(String term) {
        return vacancyRepository.findTitleByPart(term + '%');
    }
}


