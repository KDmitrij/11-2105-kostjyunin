package filter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/api/*")
public class apiFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) resp;
        response.addHeader("Access-Control-Allow-Origin","*");
        response.addHeader("Access-Control-Request-Methods","POST,GET,PUT,DELETE,OPTIONS");
        response.addHeader("Access-Control-Allow-Methods","POST,GET,PUT,DELETE");
        response.addHeader("Access-Control-Allow-Headers","X-Requested-With, content-type");
        chain.doFilter(req,response);
    }

    @Override
    public void destroy() {
    }

}
