package viewobject;

public class VacancyVO {
    private Long id;
    private String title;
    private int experience;
    private String city;

    public VacancyVO(Long id, String title, int experience, String city) {
        this.id = id;
        this.title = title;
        this.experience = experience;
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }
}