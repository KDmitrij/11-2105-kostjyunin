package viewobject;

public class CVVO {
    private int experience;
    private Long id;
    private String title;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String city;

    public CVVO(int experience, Long id, String title, String city) {
        this.experience = experience;
        this.id = id;
        this.title = title;
        this.city = city;
    }

    public int getExperience() {

        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String label) {
        this.title = label;
    }
}