package repository;

import model.Invite;
import model.InvitePK;
import model.Vacancy;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.ManyToOne;

public interface InviteRepository extends CrudRepository<Invite, InvitePK> {
    @Transactional
    @Query("select v from Invite v where v.key.vacancy   = ?1")
    Iterable<Invite> getInvites(Vacancy v);
}
