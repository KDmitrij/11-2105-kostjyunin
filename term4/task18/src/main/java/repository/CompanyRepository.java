package repository;

import model.Company;
import model.Vacancy;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface CompanyRepository extends CrudRepository<Company, Long> {
    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("select v from Vacancy v where v.company = ?1")
    Iterable<Vacancy> getVacancies(Company l);
}
