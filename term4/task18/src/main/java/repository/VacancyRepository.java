package repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import model.Vacancy;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface VacancyRepository extends CrudRepository<Vacancy, Long>, VacancyRepositoryCustom {
    List<Vacancy> findByTitle(String title);

    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query("update Vacancy vacancy set vacancy.title = ?2 where vacancy.title LIKE ?1")
    int setNewTitle(String title, String newTitle);

    @Transactional
    @Query("select vacancy from Vacancy vacancy where vacancy.category.id = ?1")
        //      SELECT * FROM vacancy, vacancy_category WHERE ... AND vacancy_category.category_id=?1
    Iterable<Vacancy> findByCategory(Long categoryID);

    Iterable<Vacancy> findByTitleStartingWithIgnoreCase(String term);

    @Transactional
    @Query("select distinct v.title from Vacancy v where v.title like ?1")
        //      SELECT * FROM cv, cv_category WHERE ... AND cv_category.category_id=?1
    List<String> findTitleByPart(String s);
}
