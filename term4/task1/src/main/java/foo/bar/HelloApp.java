package foo.bar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Scanner;

public class HelloApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        System.out.println("Вопросы-ответы, погнали...");
        Scanner s = new Scanner(System.in);
        System.out.println("Вы мужчина(М) или женщина(Ж):");
        Human u = (Human)context.getBean(s.next());
        u.test();
        System.out.println(u);
    }

}
