package service.proxy;

import service.OrderService;

public class OrderServiceProxy implements OrderService{
    private OrderService myLittleProxy;

    public OrderServiceProxy(OrderService myLittleProxy) {
        this.myLittleProxy = myLittleProxy;
    }

    @Override
    public String getOrder(String orderNumber) {
        System.out.println("Method : getOrder");
        System.out.println("Params : [(String)orderNumber:" + orderNumber + "]");
        try {
            return myLittleProxy.getOrder(orderNumber);
        } catch (Exception e) {
            System.err.print("Exception:");
            e.printStackTrace();
        }
            return null;
    }

    @Override
    public String addOrder(String orderNumber, Long amount) {
        System.out.println("Method : addOrder");
        System.out.println("Params : [(String)orderNumber:" + orderNumber + ", (Long)amount:" + amount + "]");
        System.out.println("Start addOrder():" + System.currentTimeMillis());
        String result = myLittleProxy.addOrder(orderNumber, amount);
        System.out.println("End addOrder():" + System.currentTimeMillis());
        return result;
    }
}
