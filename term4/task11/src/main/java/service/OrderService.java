package service;

public interface OrderService {

    String getOrder(String orderNumber) throws Exception;

    String addOrder(String orderNumber, Long amount);

}
