package controller;

import controller.editor.CategoryEditor;
import model.Vacancy;
import model.Category;
import model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import service.SearchService;
import service.CompanyService;

@Controller
public class VacancyController {

    private CompanyService companyService;
    private SearchService searchService;

    @Autowired
    public VacancyController(CompanyService companyService, SearchService searchService) {
        this.companyService = companyService;
        this.searchService = searchService;
    }


    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Category.class, new CategoryEditor());
    }

    @RequestMapping("/vacancy/{id}")
    public ModelAndView getVacancy(@PathVariable Long id) {
        return new ModelAndView("vacancy_page", "vacancy", companyService.getVacancyById(id));
    }

    @RequestMapping("/vacancy/list")
    public ModelAndView getVacancy() {
        ModelAndView mv = new ModelAndView("vacancy_list");
        mv.addObject("vacancyList", companyService.getAllVacancies());
        mv.addObject("category", 0);
        mv.addObject("allCategories", searchService.getAllCategories());
        return mv;
    }

    @RequestMapping("/vacancy/list/{categoryId}")
    public ModelAndView getVacancyByCategory(@PathVariable Long categoryId) {
        ModelAndView mv = new ModelAndView("vacancy_list");
        mv.addObject("vacancyList", companyService.getVacancyListByCategoryId(categoryId));
        mv.addObject("category", categoryId);
        mv.addObject("allCategories", searchService.getAllCategories());
        return mv;
    }

    @RequestMapping("/vacancy/edit/{id}")
    public ModelAndView editVacancy(@PathVariable Long id) {
        ModelAndView mv = new ModelAndView("vacancy_edit");
        mv.addObject("vacancy", companyService.getVacancyById(id));
        mv.addObject("allCategories", searchService.getAllCategories());
        return mv;
    }

    @RequestMapping("/vacancy/create")
    public ModelAndView editVacancy() {
        ModelAndView mv = new ModelAndView("vacancy_edit");
        mv.addObject("vacancy", new Vacancy());
        mv.addObject("allCategories", searchService.getAllCategories());
        return mv;
    }

    @RequestMapping("/vacancy/save")
    public String saveVacancy(Vacancy vacancy) {
        vacancy.setOwner(new Company(1L));
        companyService.saveVacancy(vacancy);
        return "redirect:/vacancy/list";
    }

}
