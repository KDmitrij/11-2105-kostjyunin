package service;

import model.Vacancy;
import model.Company;

public interface CompanyService {
    Vacancy getVacancyById(Long id);

    Company getCompanyById(Long id);

    Iterable<Vacancy> getAllVacancies();

    void saveVacancy(Vacancy vacancy);

    Iterable<Vacancy> getVacancyListByCategoryId(Long categoryID);

    

}
