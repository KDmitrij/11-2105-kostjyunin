package foo.bar.cfg;
import foo.bar.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedList;


@Configuration
@ComponentScan("foo.bar")
public class MainConfig {
      @Bean
      public Woman Ж(){
          Woman girl = new Woman();
          LinkedList<Hand> hands = new LinkedList<>();
          hands.add(right());
          hands.add(left());
          girl.setHands(hands);
          return girl;
      }
    @Bean
      public Man М(){
          Man boy = new Man();
          LinkedList<Hand> hands = new LinkedList<>();
          hands.add(right());
          hands.add(left());
          boy.setHands(hands);
          return boy;
      }
    private Hand right(){
       Hand rightHand = new Hand();
       rightHand.setName("Правая рука");
        return rightHand;
    }
    private Hand left(){
       Hand leftHand = new Hand();
       leftHand.setName("Левая рука");
        return leftHand;
    }
}
