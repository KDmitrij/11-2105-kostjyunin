package foo.bar;

import foo.bar.cfg.MainConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

public class HelloApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(MainConfig.class);
        context.refresh();
        System.out.println("Вопросы-ответы, погнали...");
        Scanner s = new Scanner(System.in);
        System.out.println("Вы мужчина(М) или женщина(Ж):");
        Human u = (Human)context.getBean(s.next());
        u.test();
        System.out.println(u);
    }

}
