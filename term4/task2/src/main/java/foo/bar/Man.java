package foo.bar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;
@Component
public class Man extends Human {
    private List hands;
    @Autowired
    @Qualifier("RightEye")
    private Eye firstEye;
    @Autowired
    @Qualifier("LeftEye")
    private Eye secondEye;
    public Man() {
        super("Мужчина");
    }
    public void test(){
        System.out.println("Тут должны быть другие вопросы, чтобы оправдать разделение на два класса, но увы...");
        System.out.println("Чему равно два плюс два, если на улице облачно?");
        Scanner s = new Scanner(System.in);
        if(s.nextInt() != 4){
            System.out.println("да уж :/");
            throw(new Error());
        }
        System.out.println("окей, кто-то проходил арифметику, не плохо.");
        System.out.println("Какова точная масса Земли, до грамма:");
        System.out.println(s.next() + "- и это твой ответ? :/");
    }

    public void setHands(List hands) {
        this.hands = hands;
    }
    public String toString() {
        return "Итак вы " + gender + " и у вас есть " + hands.get(0) + " и " + hands.get(1) + ",a также " + firstEye.getName() +
                " и " + secondEye.getName();
    }
}
