package service;

import model.Company;
import model.Vacancy;

public interface CompanyService {
    Company getCompanyById(Long id);
    Vacancy getVacancyById(Long id);

}
