package Dao;

import java.util.Date;

public class Resume {
    private User user;
    private String title;
    private Date experience;
    private String text;

    public User getIdUser() {
        return user;
    }

    public void setIdUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getExperience() {
        return experience;
    }

    public void setExperience(Date experience) {
        this.experience = experience;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
