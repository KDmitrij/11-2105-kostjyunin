package Dao;

public class Invite {
    private ResumeVacancy idFirst;
    private ResumeVacancy idSecond;
    private byte type;

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public ResumeVacancy getIdSecond() {
        return idSecond;
    }

    public void setIdSecond(ResumeVacancy idSecond) {
        this.idSecond = idSecond;
    }

    public ResumeVacancy getIdFirst() {
        return idFirst;
    }

    public void setIdFirst(ResumeVacancy idFirst) {
        this.idFirst = idFirst;
    }
}
