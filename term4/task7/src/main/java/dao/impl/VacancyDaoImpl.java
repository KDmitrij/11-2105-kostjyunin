package dao.impl;


import dao.VacancyDao;
import model.Vacancy;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VacancyDaoImpl extends CRUDDaoImpl<Vacancy> implements VacancyDao{
    @Autowired
    public VacancyDaoImpl(SessionFactory sessionFactory) {
        super(Vacancy.class, sessionFactory);
    }
    @Override
    public List<Vacancy> findVacanciesByOwner(Long owner) {
        return getSession().createQuery("from Vacancy cv where cv.company.id = ?")
                .setParameter(0, owner).list();
    }
}

