package dao.impl;

import dao.ManDao;
import model.Man;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ManDaoImpl extends CRUDDaoImpl<Man> implements ManDao {
    @Autowired
    public ManDaoImpl(SessionFactory sessionFactory) {
        super(Man.class, sessionFactory);
    }
}