package dao.impl;

import dao.InviteDao;
import model.Invite;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InviteDaoImpl extends CRUDDaoImpl<Invite> implements InviteDao{
    @Autowired
    public InviteDaoImpl(SessionFactory sessionFactory) {
        super(Invite.class, sessionFactory);
    }
}
