package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name= "cv")
public class CV {
    private Man man;
    private String title;
    private Date experience;
    private String text;
    @OneToMany
    @JoinColumn(name = "id_resume")
    public List<Invite> getInvites() {
        return invites;
    }

    public void setInvites(List<Invite> invites) {
        this.invites = invites;
    }

    private List<Invite> invites;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_man")
    public Man getMan() {
        return man;
    }

    public void setMan(Man man) {
        this.man = man;
    }
    @Column(name="title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Column(name="experience")
    public Date getExperience() {
        return experience;
    }

    public void setExperience(Date experience) {
        this.experience = experience;
    }
    @Column(name="text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private long id;
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="id_resume")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CV{" +
                "man=" + man +
                ", title='" + title + '\'' +
                ", experience=" + experience +
                ", text='" + text + '\'' +
                ", id=" + id +
                '}';
    }
}
