package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name= "invite")
public class Invite {
    private CV resume;
    private Vacancy vacancy;
    private byte type;
    @Column(name="type")
    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }
    @ManyToOne
    @JoinColumn(name = "id_vacancy")
    public Vacancy getVacancy() {
        return vacancy;
    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }
    @ManyToOne
    @JoinColumn(name = "id_resume")
    public CV getCV() {
        return resume;
    }

    public void setCV(CV resume) {
        this.resume = resume;
    }

    private Long id;

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name= "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Override
    public String toString() {
        return "Invite{" +
                "resume=" + resume +
                ", vacancy=" + vacancy +
                ", type=" + type +
                ", id='" + id + '\'' +
                '}';
    }
}
