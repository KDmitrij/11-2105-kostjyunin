package service;

import model.CV;

import java.util.List;

public interface CVService extends MainService<CV> {
    public List<CV> getByOwnerId(Long owner);
}
