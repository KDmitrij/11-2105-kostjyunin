package service.impl;

import dao.ManDao;
import model.Man;
import org.springframework.stereotype.Service;
import service.ManService;
@Service
public class ManServiceImpl extends MainServiceImpl<Man, ManDao> implements ManService{}