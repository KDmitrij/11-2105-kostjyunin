package service.impl;

import dao.InviteDao;
import model.Invite;
import org.springframework.stereotype.Service;
import service.InviteService;
@Service
public class InviteServiceImpl extends MainServiceImpl<Invite, InviteDao> implements InviteService {}
