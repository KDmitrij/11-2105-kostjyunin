package service.impl;

import dao.CompanyDao;
import model.Company;
import org.springframework.stereotype.Service;
import service.CompanyService;
@Service
public class CompanyServiceImpl extends MainServiceImpl<Company, CompanyDao> implements CompanyService{}