package service.impl;

import dao.CRUDDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import service.MainService;

import java.sql.SQLException;
import java.util.List;

@org.springframework.stereotype.Service
public class MainServiceImpl<T, V extends CRUDDao<T>>implements MainService<T> {

    @Autowired
    private V DAO;

    @Transactional
    public void add(T entity)  throws SQLException{
        DAO.save(entity);
    }

    @Transactional
    public List<T> list() throws SQLException{
        return DAO.findAll();
    }

    @Transactional
    public void remove(T obj) throws SQLException {
        DAO.delete(obj);
    }
}