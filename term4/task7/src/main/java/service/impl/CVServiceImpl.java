package service.impl;

import dao.CVDao;
import model.CV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import service.CVService;

import java.util.List;
@Service
public class CVServiceImpl extends MainServiceImpl<CV, CVDao> implements CVService{
    @Autowired
    private CVDao dao;
    @Transactional
    public List<CV> getByOwnerId(Long owner) {
        return dao.findCVsByOwner(owner);
    }
}