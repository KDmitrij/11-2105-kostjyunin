package service.impl;

import dao.VacancyDao;
import model.Vacancy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import service.VacancyService;

import java.util.List;
@Service
public class VacancyServiceImpl extends MainServiceImpl<Vacancy, VacancyDao> implements VacancyService{
    @Autowired
    private VacancyDao dao;
    @Transactional
    public List<Vacancy> getByOwnerId(Long owner) {
        return dao.findVacanciesByOwner(owner);
    }
}