package service;

import java.sql.SQLException;
import java.util.List;

public interface MainService<T> {
    public void add(T entity) throws SQLException;

    public List<T> list() throws SQLException;

    public void remove(T obj) throws SQLException;
}
