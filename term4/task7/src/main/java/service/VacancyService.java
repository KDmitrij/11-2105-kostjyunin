package service;
import model.Vacancy;
import java.util.List;
public interface VacancyService extends MainService<Vacancy> {
    public List<Vacancy> getByOwnerId(Long owner);
}
