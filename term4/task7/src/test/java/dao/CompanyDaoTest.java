package dao;

import config.PersistenceConfig;
import model.Company;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        PersistenceConfig.class})
public class CompanyDaoTest {

    @Autowired
    private CompanyDao manDao;

    private Company createCompany() {
        Company user = new Company();
        user.setName("Apple");
        return user;
    }

    @Test
    public void testFindAll() throws SQLException {
        Company newCompany = createCompany();
        //create
        manDao.save(newCompany);
        List<Company> cvList = manDao.findAll();
        assertNotNull(cvList);
        assertFalse(cvList.isEmpty());
        for(Company cv: cvList) {
            assertNotNull(cv);
            assertNotNull(cv.getId());
        }
    }


    @Test
    public void testCRUD() throws SQLException {
        Company cv = createCompany();
        //create
        manDao.save(cv);
        assertEquals(manDao.findById(cv.getId()).getName(), "Apple");
        cv.setName("Samsung");
        //update
        manDao.save(cv);
        assertEquals(manDao.findById(cv.getId()).getName(), "Samsung");
        //delete
        manDao.delete(cv);
        assertNull(manDao.findById(cv.getId()));
    }
}
