package dao;

import config.PersistenceConfig;
import model.Invite;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        PersistenceConfig.class})
public class InviteDaoTest {

    @Autowired
    private InviteDao manDao;

    private Invite createInvite() {
        Invite user = new Invite();
        user.setType((byte)0);
        return user;
    }

    @Test
    public void testFindAll() throws SQLException {
        Invite newInvite = createInvite();
        //create
        manDao.save(newInvite);
        List<Invite> cvList = manDao.findAll();
        assertNotNull(cvList);
        assertFalse(cvList.isEmpty());
        for(Invite cv: cvList) {
            assertNotNull(cv);
            assertNotNull(cv.getId());
        }
    }


    @Test
    public void testCRUD() throws SQLException {
        Invite cv = createInvite();
        //create
        manDao.save(cv);
        assertEquals(manDao.findById(cv.getId()).getType(), (byte)0);
        cv.setType((byte)1);
        //update
        manDao.save(cv);
        assertEquals(manDao.findById(cv.getId()).getType(),(byte)1);
        //delete
        manDao.delete(cv);
        assertNull(manDao.findById(cv.getId()));
    }
}
