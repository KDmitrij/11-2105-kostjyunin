package dao;

import config.PersistenceConfig;
import model.Company;
import model.Vacancy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        PersistenceConfig.class})
public class VacancyDaoTest {

    @Autowired
    private VacancyDao vacancyDao;

    private Vacancy createVacancy() {
        Vacancy cv = new Vacancy();
        Company company = new Company();
        company.setName("Ann");
        cv.setCompany(company);
        cv.setText("Python, Java, Ruby");
        cv.setTitle("Software developer");
        return cv;
    }

    @Test
    public void testFindAll() throws SQLException {
        Vacancy newVacancy = createVacancy();
        //create
        vacancyDao.save(newVacancy);
        List<Vacancy> cvList = vacancyDao.findAll();
        assertNotNull(cvList);
        assertFalse(cvList.isEmpty());
        for(Vacancy cv: cvList) {
            assertNotNull(cv);
            assertNotNull(cv.getId());
        }
    }

    @Test
    public void testFindByOwner() throws SQLException {
        Vacancy newVacancy = createVacancy();
        //create
        vacancyDao.save(newVacancy);

        List<Vacancy> cvList = vacancyDao.findVacanciesByOwner(newVacancy.getCompany().getId());
        assertNotNull(cvList);
        assertFalse(cvList.isEmpty());
        for(Vacancy cv: cvList) {
            assertNotNull(cv);
            assertNotNull(cv.getId());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        Vacancy cv = createVacancy();
        //create
        vacancyDao.save(cv);
        assertEquals(vacancyDao.findById(cv.getId()).getTitle(), "Software developer");
        cv.setTitle("Programmer");
        //update
        vacancyDao.save(cv);
        assertEquals(vacancyDao.findById(cv.getId()).getTitle(), "Programmer");
        //delete
        vacancyDao.delete(cv);
        assertNull(vacancyDao.findById(cv.getId()));
    }
}
