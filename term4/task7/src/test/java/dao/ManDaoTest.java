package dao;

import config.PersistenceConfig;
import model.Man;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        PersistenceConfig.class})
public class ManDaoTest {

    @Autowired
    private ManDao manDao;

    private Man createMan() {
        Man user = new Man();
        user.setName("Ann");
        return user;
    }

    @Test
    public void testFindAll() throws SQLException {
        Man newMan = createMan();
        //create
        manDao.save(newMan);
        List<Man> cvList = manDao.findAll();
        assertNotNull(cvList);
        assertFalse(cvList.isEmpty());
        for(Man cv: cvList) {
            assertNotNull(cv);
            assertNotNull(cv.getId());
        }
    }


    @Test
    public void testCRUD() throws SQLException {
        Man cv = createMan();
        //create
        manDao.save(cv);
        assertEquals(manDao.findById(cv.getId()).getName(), "Ann");
        cv.setName("Alex");
        //update
        manDao.save(cv);
        assertEquals(manDao.findById(cv.getId()).getName(), "Alex");
        //delete
        manDao.delete(cv);
        assertNull(manDao.findById(cv.getId()));
    }
}
