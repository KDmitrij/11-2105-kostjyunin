package dao;

import config.PersistenceConfig;
import model.CV;
import model.Man;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        PersistenceConfig.class})
public class CVDaoTest {

    @Autowired
    private CVDao cvDao;

    private CV createCV() {
        CV cv = new CV();
        Man user = new Man();
        user.setName("Ann");
        cv.setMan(user);
        cv.setText("Python, Java, Ruby");
        cv.setTitle("Software developer");
        return cv;
    }

    @Test
    public void testFindAll() throws SQLException {
        CV newCV = createCV();
        //create
        cvDao.save(newCV);
        List<CV> cvList = cvDao.findAll();
        assertNotNull(cvList);
        assertFalse(cvList.isEmpty());
        for(CV cv: cvList) {
            assertNotNull(cv);
            assertNotNull(cv.getId());
        }
    }

    @Test
    public void testFindByOwner() throws SQLException {
        CV newCV = createCV();
        //create
        cvDao.save(newCV);

        List<CV> cvList = cvDao.findCVsByOwner(newCV.getMan().getId());
        assertNotNull(cvList);
        assertFalse(cvList.isEmpty());
        for(CV cv: cvList) {
            assertNotNull(cv);
            assertNotNull(cv.getId());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        CV cv = createCV();
        //create
        cvDao.save(cv);
        assertEquals(cvDao.findById(cv.getId()).getTitle(), "Software developer");
        cv.setTitle("Programmer");
        //update
        cvDao.save(cv);
        assertEquals(cvDao.findById(cv.getId()).getTitle(), "Programmer");
        //delete
        cvDao.delete(cv);
        assertNull(cvDao.findById(cv.getId()));
    }
}
