package core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.OrderService;

public class App {
	public static void main(String[] args){

		ApplicationContext appContext = new ClassPathXmlApplicationContext("Spring-Customer.xml");

		/**CustomerService customer = (CustomerService) appContext.getBean(CustomerService.class);
	    customer.addCustomer();
		
		customer.addCustomerReturnValue();
		
		customer.addCustomerAround("customer#1");

        customer.addCustomerThrowException();    **/

		OrderService order = (OrderService) appContext.getBean(OrderService.class);
		order.addOrder("what?", 9L);
	    try{
            order.getOrder("what?");
        }catch (Exception e){
            System.out.println("err");
        }

	}
}