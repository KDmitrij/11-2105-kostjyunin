package aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

import java.util.Arrays;

@Aspect
public class LoggingAspect {

    @Before("execution(* service.CustomerService.addCustomer(..))")
    public void logBefore(JoinPoint joinPoint) {

        System.out.println("logBefore() is running!");
        System.out.println("hijacked : " + joinPoint.getSignature().getName());
        System.out.println("******");
    }

    @After("execution(* service.CustomerService.addCustomer(..))")
    public void logAfter(JoinPoint joinPoint) {

        System.out.println("logAfter() is running!");
        System.out.println("hijacked : " + joinPoint.getSignature().getName());
        System.out.println("******");

    }

    @AfterReturning(
            pointcut = "execution(* service.CustomerService.addCustomerReturnValue(..))",
            returning = "result")
    public void logAfterReturning(JoinPoint joinPoint, Object result) {

        System.out.println("logAfterReturning() is running!");
        System.out.println("hijacked : " + joinPoint.getSignature().getName());
        System.out.println("Method returned value is : " + result);
        System.out.println("******");

    }

    @AfterThrowing(
            pointcut = "execution(* service.CustomerService.addCustomerThrowException(..)) || execution(* service.OrderService.getOrder(..)))",
            throwing = "error")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {

        System.out.println("logAfterThrowing() is running!");
        System.out.println("hijacked : " + joinPoint.getSignature().getName());
        System.out.println("Exception : " + error);
        System.out.println("******");

    }


    @Around("execution(* service.CustomerService.addCustomerAround(..)) || execution(* service.OrderService.*(..))")
    public void logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        if (joinPoint.toString().contains("addOrder")) {
            System.out.println(System.currentTimeMillis() + " - start");
            System.out.println("******");
        }
        System.out.println("logAround() is running!");
        System.out.println("hijacked method : " + joinPoint.getSignature().getName());
        System.out.println("hijacked arguments : " + Arrays.toString(joinPoint.getArgs()));

        System.out.println("Around before is running!");

        System.out.println("******");
        joinPoint.proceed();
        System.out.println("******");
        if (joinPoint.toString().contains("addOrder")) {
            System.out.println(System.currentTimeMillis() + " - end");
        } else {
            System.out.println("Around after is running!");
        }
        System.out.println("******");

    }

}
