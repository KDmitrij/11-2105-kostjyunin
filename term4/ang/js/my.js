var myApp = angular.module('myApp', []);

myApp.controller('allCtrl', function ($scope, $http) {
    //FIELDS
    $scope.company = {
    id : 1,
    name : '',
    city : '',
    email : '',
    description : '',
    password : ''}

    //METHODS
    $scope.get = function(){
        $http({method : 'GET', url: "http://localhost:8080/api/company/" + $scope.company.id}).success(
            function(result){
                $scope.company = result;
            }).error(
                function(data, status){
                    if(status == 404){
                        alert("Компании с таким id не существует")
                    }
                }
            )};


    $scope.delete = function(){
        $http({method : 'DELETE', url: "http://localhost:8080/api/company/" + $scope.company.id}).success(
            function(){
            $scope.company = {
                id : 1,
                name : '',
                city : '',
                email : '',
                description : '',
                password : ''}
            }
        )
    }


    $scope.post = function() {
        var a = $scope.company.id;
        $scope.company.id = undefined;
        $http({method : 'POST', url: "http://localhost:8080/api/company/", data: $scope.company})
        $scope.company.id = a;
    };


    $scope.put = function() {
        $http({method : 'PUT', url: "http://localhost:8080/api/company/" + $scope.company.id, data:$scope.company})}
});