package repository;

import config.PersistenceTestConfig;
import model.Question;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;

import static org.junit.Assert.*;
import static repository.fixture.TestConstants.QuestionConstants.QUESTION_TEXT;
import static repository.fixture.TestData.standardQuestion;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PersistenceTestConfig.class})
public class QuestionRepositoryTest {

    @Autowired
    private QuestionRepository questionRepository;

    @Test
    public void testFindAll() throws SQLException {
        Question newQuestion = standardQuestion();
        questionRepository.save(newQuestion);
        Iterable<Question> questionList = questionRepository.findAll();
        assertNotNull(questionList);
        assertTrue(questionList.iterator().hasNext());
        for(Question question: questionList) {
            assertNotNull(question);
            assertNotNull(question.getId());
        }
        questionRepository.deleteAll();
    }

    @Test
    public void testCRUD(){
        Question question = standardQuestion();
        questionRepository.save(question);
        question = questionRepository.findOne(question.getId());
        assertEquals(question.getText(), QUESTION_TEXT);
        assertEquals(question.getId(), standardQuestion().getId());
        question.setText("Саша");
        questionRepository.save(question);
        assertEquals(questionRepository.findOne(question.getId()).getText(), "Саша");
        questionRepository.delete(question);
        assertNull(questionRepository.findOne(question.getId()));
    }

}
