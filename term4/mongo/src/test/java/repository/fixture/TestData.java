package repository.fixture;

import model.Answer;
import model.Question;
import model.User;

import java.math.BigInteger;

import static repository.fixture.TestConstants.UserConstants.USER_CITY;
import static repository.fixture.TestConstants.UserConstants.USER_NAME;
import static repository.fixture.TestConstants.QuestionConstants.QUESTION_TEXT;

public class TestData {

    public static User standardUser() {
        User user = new User();
        user.setId(new BigInteger("1"));
        user.setName(USER_NAME);
        user.setCity(USER_CITY);
        user.setEmail("alex@gmail.com");
        user.setPassword("qwe");
        user.setSalt("rty");
        return user;
    }
    public static Question standardQuestion() {
        Question question = new Question();
        question.setId(new BigInteger("1"));
        question.setText(QUESTION_TEXT);
        question.setAnswers(new Answer[]{new Answer("1","good",true),});
        return question;
    }

}
