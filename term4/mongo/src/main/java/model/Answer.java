package model;

import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigInteger;

public class Answer {
    private String number;
    private String text;
    private boolean correct;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public String getNumber() {

        return number;
    }

    public void setNumber(String id) {
        this.number = id;
    }

    public Answer(String id, String text, boolean correct) {
        this.number = id;
        this.text = text;
        this.correct = correct;
    }

    public Answer() {
    }

    @Override
    public String toString() {
        return "Вопрос №" + number +
                "\n Текст: '" + text + '\'' +
                (correct?"\n ܘ Лучший ответ":"");
    }
}
