package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import service.QuestionService;

import java.math.BigInteger;

@Controller
public class QuestionProfileController {

    private QuestionService questionService;

    @Autowired
    public QuestionProfileController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @RequestMapping("/question/{id}")
    public ModelAndView getQuestion(@PathVariable BigInteger id) {
        return new ModelAndView("question_profile", "question", questionService.getQuestionById(id));
    }
}
