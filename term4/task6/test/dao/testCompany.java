package dao;

import model.Company;
import model.Man;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


public class testCompany {

    private CompanyDao companyDao = Factory.getInstance().getCompanyDao();

    private Company createCompany() {
        Company company = new Company();
        company.setName("Apple");
        return company;
    }

    @Test
    public void testFindAll() throws SQLException {
        Company r = createCompany();
        companyDao.addCompany(r);
        List<Company> companyList = companyDao.getAllCompanies();
        assertNotNull(companyList);
        assertFalse(companyList.isEmpty());
        for(Company company: companyList) {
            assertNotNull(company);
            assertNotNull(company.getId());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        Company company = createCompany();
        companyDao.addCompany(company);
        assertEquals(companyDao.getCompanyById(company.getId()).getName(), "Apple");
        company.setName("AppleCorp");
        companyDao.updateCompany(company);
        assertEquals(companyDao.getCompanyById(company.getId()).getName(), "AppleCorp");
        companyDao.deleteCompany(company);
        assertNull(companyDao.getCompanyById(company.getId()));
    }
}