package dao;

import model.Company;
import model.CV;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


public class testCV {

    private CVDao vacancyDao = Factory.getInstance().getCVDao();

    private CV createCV() {
        CV vacancy = new CV();
        Company company = new Company();
        company.setName("Apple");
        vacancy.setText("Python, Java, Ruby");
        vacancy.setTitle("Software developer");
        return vacancy;
    }

    @Test
    public void testFindAll() throws SQLException {
        CV r = createCV();
        vacancyDao.addCV(r);
        List<CV> vacancyList = vacancyDao.getAllVacancies();
        assertNotNull(vacancyList);
        assertFalse(vacancyList.isEmpty());
        for(CV vacancy: vacancyList) {
            assertNotNull(vacancy);
            assertNotNull(vacancy.getId());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        CV vacancy = createCV();
        vacancyDao.addCV(vacancy);
        assertEquals(vacancyDao.getCVById(vacancy.getId()).getTitle(), "Software developer");
        vacancy.setTitle("Programmer");
        vacancyDao.updateCV(vacancy);
        assertEquals(vacancyDao.getCVById(vacancy.getId()).getTitle(), "Programmer");
        vacancyDao.deleteCV(vacancy);
        assertNull(vacancyDao.getCVById(vacancy.getId()));
    }
}