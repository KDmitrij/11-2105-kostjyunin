package dao;

        import model.Resume;
        import model.Man;
        import org.junit.Test;

        import java.sql.SQLException;
        import java.util.List;

        import static org.junit.Assert.*;


public class testResume {

    private ResumeDao resumeDao = Factory.getInstance().getResumeDao();

    private Resume createResume() {
        Resume resume = new Resume();
        Man man = new Man();
        man.setName("Ann");
        resume.setText("Python, Java, Ruby");
        resume.setTitle("Software developer");
        return resume;
    }

    @Test
    public void testFindAll() throws SQLException {
        Resume r = createResume();
        resumeDao.addResume(r);
        List<Resume> resumeList = resumeDao.getAllResumes();
        assertNotNull(resumeList);
        assertFalse(resumeList.isEmpty());
        for(Resume resume: resumeList) {
            assertNotNull(resume);
            assertNotNull(resume.getId());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        Resume resume = createResume();
        resumeDao.addResume(resume);
        assertEquals(resumeDao.getResumeById(resume.getId()).getTitle(), "Software developer");
        resume.setTitle("Programmer");
        resumeDao.updateResume(resume);
        assertEquals(resumeDao.getResumeById(resume.getId()).getTitle(), "Programmer");
        resumeDao.deleteResume(resume);
        assertNull(resumeDao.getResumeById(resume.getId()));
    }
}