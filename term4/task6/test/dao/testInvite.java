package dao;

import model.Invite;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


public class testInvite {

    private InviteDao inviteDao = Factory.getInstance().getInviteDao();

    private Invite createInvite() {
        Invite invite = new Invite();
        invite.setType((byte) 0);
        return invite;
    }

    @Test
    public void testFindAll() throws SQLException {
        Invite r = createInvite();
        inviteDao.addInvite(r);
        List<Invite> inviteList = inviteDao.getAllInvites();
        assertNotNull(inviteList);
        assertFalse(inviteList.isEmpty());
        for(Invite invite: inviteList) {
            assertNotNull(invite);
            assertNotNull(invite.getId());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        Invite invite = createInvite();
        inviteDao.addInvite(invite);
        assertEquals(inviteDao.getInviteById(invite.getId()).getType(), (byte)0);
        invite.setType((byte) 1);
        inviteDao.updateInvite(invite);
        assertEquals(inviteDao.getInviteById(invite.getId()).getType(), (byte) 1);
        inviteDao.deleteInvite(invite);
        assertNull(inviteDao.getInviteById(invite.getId()));
    }
}