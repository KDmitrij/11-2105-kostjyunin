package dao;

import model.Man;
import model.Man;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;


public class testMan {

    private ManDao manDao = Factory.getInstance().getManDao();

    private Man createMan() {
        Man man = new Man();
        man.setName("Ivan");
        return man;
    }

    @Test
    public void testFindAll() throws SQLException {
        Man r = createMan();
        manDao.addMan(r);
        List<Man> manList = manDao.getAllMans();
        assertNotNull(manList);
        assertFalse(manList.isEmpty());
        for(Man man: manList) {
            assertNotNull(man);
            assertNotNull(man.getId());
        }
    }

    @Test
    public void testCRUD() throws SQLException {
        Man man = createMan();
        manDao.addMan(man);
        assertEquals(manDao.getManById(man.getId()).getName(), "Ivan");
        man.setName("Vanya");
        manDao.updateMan(man);
        assertEquals(manDao.getManById(man.getId()).getName(), "Vanya");
        manDao.deleteMan(man);
        assertNull(manDao.getManById(man.getId()));
    }
}