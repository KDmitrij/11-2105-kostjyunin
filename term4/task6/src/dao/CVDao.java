package dao;

import model.CV;

public interface CVDao {
    abstract void addCV(CV vacancy) throws java.sql.SQLException;

    void updateCV(CV vacancy) throws java.sql.SQLException;

    CV getCVById(Long aLong) throws java.sql.SQLException;

    java.util.List getAllVacancies() throws java.sql.SQLException;

    void deleteCV(CV vacancy) throws java.sql.SQLException;
}
