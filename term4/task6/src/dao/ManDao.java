package dao;

import model.Man;


public interface ManDao {
        abstract void addMan(Man man) throws java.sql.SQLException;

        void updateMan(Man man) throws java.sql.SQLException;

        Man getManById(Long aLong) throws java.sql.SQLException;

        java.util.List getAllMans() throws java.sql.SQLException;

        void deleteMan(Man man) throws java.sql.SQLException;
}
