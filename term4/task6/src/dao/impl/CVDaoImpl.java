package dao.impl;

import dao.CVDao;
import model.CV;
import org.hibernate.Session;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CVDaoImpl implements CVDao {
    public void addCV(CV vacancy) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(vacancy);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе CVDaoImpl");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateCV(CV vacancy) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(vacancy);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе updateCV");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public CV getCVById(Long id) throws SQLException {
        Session session = null;
        CV vacancy = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            vacancy = (CV) session.get(CV.class, id);
        } catch (Exception e) {
            System.err.println("Ошибка в методе getCVById");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return vacancy;
    }

    public List<CV> getAllVacancies() throws SQLException {
        Session session = null;
        List<CV> users = new ArrayList<CV>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            users = session.createCriteria(CV.class).list();
        } catch (Exception e) {
            System.err.println("Ошибка в методе getAllCompanies");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return users;
    }

    public void deleteCV(CV vacancy) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(vacancy);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе deleteCV");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
