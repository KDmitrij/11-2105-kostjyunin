package dao.impl;


import model.Resume;
import org.hibernate.Session;
import util.HibernateUtil;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResumeDaoImpl implements dao.ResumeDao {
    public void addResume(Resume resume) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(resume);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе ResumeDaoImpl");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateResume(Resume resume) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(resume);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе updateResume");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public Resume getResumeById(Long id) throws SQLException {
        Session session = null;
        Resume resume = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            resume = (Resume) session.get(Resume.class, id);
        } catch (Exception e) {
            System.err.println("Ошибка в методе getResumeById");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return resume;
    }

    public List<Resume> getAllResumes() throws SQLException {
        Session session = null;
        List<Resume> users = new ArrayList<Resume>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            users = session.createCriteria(Resume.class).list();
        } catch (Exception e) {
            System.err.println("Ошибка в методе getAllResumes");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return users;
    }

    public void deleteResume(Resume resume) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(resume);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе deleteResume");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}

