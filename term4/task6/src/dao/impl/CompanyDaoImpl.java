package dao.impl;

import model.Company;
import org.hibernate.Session;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CompanyDaoImpl implements dao.CompanyDao {
    public void addCompany(Company company) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(company);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе CompanyDaoImpl");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateCompany(Company company) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(company);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе updateCompany");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public Company getCompanyById(Long id) throws SQLException {
        Session session = null;
        Company company = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            company = (Company) session.get(Company.class, id);
        } catch (Exception e) {
            System.err.println("Ошибка в методе getCompanyById");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return company;
    }

    public List<Company> getAllCompanies() throws SQLException {
        Session session = null;
        List<Company> users = new ArrayList<Company>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            users = session.createCriteria(Company.class).list();
        } catch (Exception e) {
            System.err.println("Ошибка в методе getAllCompanies");
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return users;
    }

    public void deleteCompany(Company company) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(company);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе deleteCompany");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
