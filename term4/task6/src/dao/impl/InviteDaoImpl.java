package dao.impl;

import dao.InviteDao;
import model.Invite;
import org.hibernate.Session;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InviteDaoImpl implements InviteDao {
    public void addInvite(Invite invite) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(invite);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе InviteDaoImpl");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateInvite(Invite invite) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(invite);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе updateInvite");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public Invite getInviteById(Long id) throws SQLException {
        Session session = null;
        Invite invite = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            invite = (Invite) session.get(Invite.class, id);
        } catch (Exception e) {
            System.err.println("Ошибка в методе getInviteById");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return invite;
    }
    public List<Invite> getAllInvites() throws SQLException {
        Session session = null;
        List<Invite> users = new ArrayList<Invite>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            users = session.createCriteria(Invite.class).list();
        } catch (Exception e) {
            System.err.println("Ошибка в методе getAllCompanies");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return users;
    }

    public void deleteInvite(Invite invite) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(invite);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("Ошибка в методе deleteInvite");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
