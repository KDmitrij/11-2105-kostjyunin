package dao;

import dao.impl.*;

public class Factory {

    private static ManDao manDAO = null;
    private static ResumeDao resumeDAO = null;
    private static CompanyDao companyDAO = null;
    private static CVDao vacancyDAO = null;
    private static InviteDao inviteDAO = null;
    private static Factory instance = null;

    public static synchronized Factory getInstance(){
        if (instance == null){
            instance = new Factory();
        }
        return instance;
    }

    public ManDao getManDao(){
        if (manDAO == null){
            manDAO = new ManDaoImpl();
        }
        return manDAO;
    }
    public ResumeDao getResumeDao(){
        if (resumeDAO == null){
            resumeDAO = new ResumeDaoImpl();
        }
        return resumeDAO;
    }
    public CompanyDao getCompanyDao(){
        if (companyDAO == null){
            companyDAO = new CompanyDaoImpl();
        }
        return companyDAO;
    }
    public CVDao getCVDao(){
        if (vacancyDAO == null){
            vacancyDAO = new CVDaoImpl();
        }
        return vacancyDAO;
    }
    public InviteDao getInviteDao(){
        if (inviteDAO == null){
            inviteDAO = new InviteDaoImpl();
        }
        return inviteDAO;
    }
}