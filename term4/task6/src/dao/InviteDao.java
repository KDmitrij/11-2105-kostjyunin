package dao;

import model.Invite;

public interface InviteDao {
    abstract void addInvite(Invite invite) throws java.sql.SQLException;

    void updateInvite(Invite invite) throws java.sql.SQLException;

    Invite getInviteById(Long aLong) throws java.sql.SQLException;

    java.util.List getAllInvites() throws java.sql.SQLException;

    void deleteInvite(Invite invite) throws java.sql.SQLException;
}
