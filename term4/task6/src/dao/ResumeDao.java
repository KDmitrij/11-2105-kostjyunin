package dao;

import model.Resume;


public interface ResumeDao {
    abstract void addResume(Resume resume) throws java.sql.SQLException;

    void updateResume(Resume resume) throws java.sql.SQLException;

    Resume getResumeById(Long aLong) throws java.sql.SQLException;

    java.util.List getAllResumes() throws java.sql.SQLException;

    void deleteResume(Resume resume) throws java.sql.SQLException;
}