package dao;

import model.Man;


public interface UserDao {
        abstract void addUser(Man man) throws java.sql.SQLException;

        void updateUser(Man man) throws java.sql.SQLException;

        Man getUserById(Long aLong) throws java.sql.SQLException;

        java.util.List getAllUsers() throws java.sql.SQLException;

        void deleteUser(Man man) throws java.sql.SQLException;
}
