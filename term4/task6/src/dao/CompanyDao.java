package dao;


import model.Company;

public interface CompanyDao {
    abstract void addCompany(Company company) throws java.sql.SQLException;

    void updateCompany(Company company) throws java.sql.SQLException;

    Company getCompanyById(Long aLong) throws java.sql.SQLException;

    java.util.List getAllCompanies() throws java.sql.SQLException;

    void deleteCompany(Company company) throws java.sql.SQLException;
}
