import dao.Factory;
import model.*;

import java.sql.SQLException;
import java.util.List;

public class Run {

    public static void main(String[] args) throws SQLException {
        List<Man> mans = Factory.getInstance().getManDao().getAllMans();
        List<Company> companies = Factory.getInstance().getCompanyDao().getAllCompanies();
        List<Invite> invites = Factory.getInstance().getInviteDao().getAllInvites();
        List<Resume> resumes = Factory.getInstance().getResumeDao().getAllResumes();
        List<CV> vacancies = Factory.getInstance().getCVDao().getAllVacancies();
        System.out.println("Все люди:");
        for (Man man : mans) {
            System.out.println(man);
            System.out.println("=============================");
        }
        System.out.println();
        System.out.println("Все компании:");
        for (Company company : companies) {
            System.out.println(company);
            System.out.println("=============================");
        }
        System.out.println();
        System.out.println("Все резюме:");
        for (Resume resume : resumes) {
            System.out.println(resume);
            System.out.println("=============================");
        }
        System.out.println();
        System.out.println("Все вакансии::");
        for (CV vacancy : vacancies) {
            System.out.println(vacancy);
            System.out.println("=============================");
        }
        System.out.println();
        System.out.println("Все Приглашения:");
        for (Invite invite : invites) {
            System.out.println(invite);
            System.out.println("=============================");
        }

    }
}
