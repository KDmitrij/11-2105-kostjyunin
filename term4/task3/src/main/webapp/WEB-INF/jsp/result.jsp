<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Spring MVC Form Handling</title>
</head>
<body>

<h2>Submitted Student Information</h2>
<a href="http://localhost:8080/task3/"><button>add</button> </a>
<table border="2px">
    <c:if test="${list != null}">
        <c:forEach var="student" items="${list}">
            <tr>
                <td>Name</td>
                <td>${student.name}</td>
                <td>Mobile</td>
                <td>${student.mobile}</td>
            </tr>
        </c:forEach>
    </c:if>

</table>
</body>
</html>