<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Mobile info</title>
</head>
<body>

<h2>Student Mobile</h2>
<form:form method="POST" action="addStudent">
    <table>
        <tr>
            <td><form:label path="name">Name</form:label></td>
            <td><form:input path="name" /></td>
        </tr>
        <tr>
            <td><form:label path="mobile">Mobile</form:label></td>
            <td><form:input path="mobile" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit"/>
            </td>
        </tr>
    </table>
    <c:if test="${list != null}">
        <c:forEach var="student" items="${list}">
            <tr>
                <td>Name</td>
                <td>${student.name}</td>
            </tr>
            <tr>
                <td>Mobile</td>
                <td>${student.mobile}</td>
            </tr>
        </c:forEach>
    </c:if>
</form:form>
</body>
</html>