package com.springapp.mvc;

public class Student {
   private Integer mobile;
   private String name;

   public void setMobile(Integer mobile) {
      this.mobile = mobile;
   }
   public Integer getMobile() {
      return mobile;
   }

   public void setName(String name) {
      this.name = name;
   }
   public String getName() {
      return name;
   }
}