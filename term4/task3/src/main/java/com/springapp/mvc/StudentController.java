package com.springapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;

import java.util.LinkedList;

@Controller
public class StudentController {
    LinkedList<Student> list = new LinkedList();

   @RequestMapping(value = "/", method = RequestMethod.GET)
   public ModelAndView student() {
      return new ModelAndView("index", "command", new Student());
   }
   
   @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
   public String addStudent(@ModelAttribute("SpringWeb")Student student, 
   ModelMap model) {
      list.add(student);
      model.addAttribute("list", list);
      return "result";
   }
}