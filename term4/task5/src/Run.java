import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;


import dao.Factory;
import model.Resume;
import model.User;

public class Run {

    public static void main(String[] args) throws SQLException {
        List<User> users = Factory.getInstance().getUserDao().getAllUsers();
        System.out.println("========Все студенты=========");
        for (User user : users) {
            System.out.println("Имя студента : " + user.getName() + ",  id : " + user.getId());
            System.out.println("=============================");
        }
        users = Factory.getInstance().getUserDao().getIvanov();
        System.out.println("Вот все Ивановы:");
        for (User user : users) {
            System.out.println("Имя студента : " + user.getName() + ",  id : " + user.getId());
            System.out.println("=============================");
        }
        System.out.println("Введите номер телефона:");
        List<Resume> resumes = Factory.getInstance().getUserDao().getResumesFirstUser();
        System.out.println("Вот все резюме пользователя 1:");
        for (Resume resume : resumes) {
            System.out.println("Название резюме : " + resume.getTitle());
            System.out.println("=============================");
        }
        resumes = Factory.getInstance().getUserDao().getResumesSecondUser();
        System.out.println("Вот все резюме пользователя 2:");
        for (Resume resume : resumes) {
            System.out.println("Название резюме : " + resume.getTitle());
            System.out.println("=============================");
        }
    }
}
