package dao;

import dao.impl.UserDaoImpl;

public class Factory {

    private static UserDao studentDAO = null;
    private static Factory instance = null;

    public static synchronized Factory getInstance(){
        if (instance == null){
            instance = new Factory();
        }
        return instance;
    }

    public UserDao getUserDao(){
        if (studentDAO == null){
            studentDAO = new UserDaoImpl();
        }
        return studentDAO;
    }
}