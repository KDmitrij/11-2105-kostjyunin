package dao;

import model.User;


public interface UserDao {
        abstract void addUser(User user) throws java.sql.SQLException;

        void updateUser(User user) throws java.sql.SQLException;

        User getUserById(Long aLong) throws java.sql.SQLException;

        java.util.List getAllUsers() throws java.sql.SQLException;

        void deleteUser(User user) throws java.sql.SQLException;

        java.util.List getIvanov() throws java.sql.SQLException;

        java.util.List getResumesFirstUser() throws java.sql.SQLException;

        java.util.List getResumesSecondUser() throws java.sql.SQLException;
}
