package model;

import javax.persistence.*;

@Entity
@Table(name= "invite")
public class Invite {
    private Resume resume;
    private Vacancy vacancy;
    private byte type;
    @Column(name="type")
    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }
    @ManyToOne
    public Vacancy getVacancy() {
        return vacancy;
    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }
    @ManyToOne
    public Resume getResume() {
        return resume;
    }

    public void setResume(Resume resume) {
        this.resume = resume;
    }

    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
