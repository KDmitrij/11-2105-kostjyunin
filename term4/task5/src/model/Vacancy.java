package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name= "vacancy")
public class Vacancy {
    private Company company;
    private String title;
    private Date experience;
    private String text;
    private int salary;
    private String city;
    @ManyToOne
    @JoinTable(name = "Company")
    public Company getIdCompany() {
        return company;
    }

    public void setIdCompany(Company company) {
        this.company = company;
    }
    @Column(name="title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Column(name="experience")
    public Date getExperience() {
        return experience;
    }

    public void setExperience(Date experience) {
        this.experience = experience;
    }
    @Column(name="text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    @Column(name="salary")
    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    @Column(name="city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private int id;

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="id_vacancy")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
